package com.gsrajeni.githubbattle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import android.os.Bundle;

import com.gsrajeni.githubbattle.core.util.LoadingDialog;
import com.gsrajeni.githubbattle.core.util.UniversalUtility;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UniversalUtility.init(this);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        NavController controller = Navigation.findNavController(findViewById(R.id.nav_host_fragment));
        Toolbar toolbar = findViewById(R.id.toolbar);
        AppBarConfiguration appBarConfiguration =
                new AppBarConfiguration.Builder(R.id.dashboardFragment, R.id.loginFragment, R.id.profileFragment).build();
        NavigationUI.setupWithNavController(toolbar, controller, appBarConfiguration);
    }
}