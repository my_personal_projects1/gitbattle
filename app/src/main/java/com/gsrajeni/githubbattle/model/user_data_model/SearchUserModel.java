package com.gsrajeni.githubbattle.model.user_data_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class SearchUserModel {

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("incomplete_results")
    @Expose
    private Boolean incompleteResults;
    @SerializedName("UserDataModels")
    @Expose
    private List<UserDataModel> UserDataModels = null;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Boolean getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(Boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<UserDataModel> getUserDataModels() {
        return UserDataModels;
    }

    public void setUserDataModels(List<UserDataModel> UserDataModels) {
        this.UserDataModels = UserDataModels;
    }

}