package com.gsrajeni.githubbattle.core.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.navigation.Navigation;
import androidx.paging.PagedList;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.core.session.SharedData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;

public class UniversalUtility {
    public static Activity mCtx;
    public static boolean isNoInternetHandled = false;

    public static void init(Activity context) {
        mCtx = context;
    }

    public static Activity getActivity(){
        return mCtx;
    }

    public static String _12Hto24H(String _12htime) {
        _12htime.replace("a.m.", "AM");
        _12htime.replace("p.m.", "PM");
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            final Date dateObj = sdf.parse(_12htime);
            return new SimpleDateFormat("HH:mm").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String getContactName(String number) {

        String name = null;

        // define the columns I want the query to return
        String[] projection = new String[] {
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};

        // encode the phone number and build the filter URI
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        // query time
        Cursor cursor = getActivity().getContentResolver().query(contactUri, projection, null, null, null);

        if(cursor != null) {
            if (cursor.moveToFirst()) {
                name =      cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            } else {
                name = "Not Found";
            }
            cursor.close();
        }
        return name;
    }

    public static String _24Hto12H(String _24htime) {
        if (_24htime.equals("00:00"))
            return _24htime;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(_24htime);
            return new SimpleDateFormat("hh:mm a").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String yyyy_mm_dd_to_dd_MMM_yyyy(String yyyy_mm_dd) {

        try {
            String pattern = "yyyy-MM-dd";
            final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            final Date dateObj = sdf.parse(yyyy_mm_dd);
            String pattern1 = "dd MMM, yyyy";
            return new SimpleDateFormat(pattern1).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
            return null;
        }
    }



    //"2019-05-14 10:58:04"
    public static String TimeStampToDate(String timestamp) {

        try {
            String pattern = "yyyy-MM-dd H:mm:ss";
            final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            final Date dateObj = sdf.parse(timestamp);
            String pattern1 = "dd MMM, yyyy";
            return new SimpleDateFormat(pattern1).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    //"2019-05-14 10:58:04"
    public static long TimeToMillis(String timestamp) {

        try {
            String pattern = "H:mm";
            final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            final Date dateObj = sdf.parse(timestamp);
            return dateObj.getTime();
        }catch (Exception e){
            return 0l;
        }
    }

    public static void setupUI(View view, final Activity activity) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView, activity);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
      /*  InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);*/

        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = activity.getCurrentFocus();
        /*
         * If no view is focused, an NPE will be thrown
         *
         * Maxim Dmitriev
         */
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

    }

    public static void handleUnsuccessfulResponse(Response<?> response) {
        switch (response.code()) {
            case 401:
                Toast.makeText(mCtx, "Unauthorized, please try again!", Toast.LENGTH_SHORT).show();
//                SharedData.logoutUser(mCtx);
                break;
            case 503:
                break;
            case 500:
                Toast.makeText(mCtx, "Internal server error.", Toast.LENGTH_SHORT).show();
                break;
            default:
                try {
                    showError(response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }


        }
        try {
            Log.d("error log", "handleErrorResponse(unsuccessful): "+ response.errorBody().string()+":"+response.raw().request().url());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void handleFailureResponse(String message, String url) {
        if (isOnline(mCtx)) {
            Toast.makeText(mCtx, "Taking too long to respond.", Toast.LENGTH_SHORT).show();
//            Snackbar.make(mCtx.findViewById(R.id.),"Taking too long to response", Snackbar.LENGTH_LONG)
//                    .show();
        } else {
            if (!isNoInternetHandled)
                handleNoInternet();
        }
        Log.d("TAG", "handleErrorResponse(failure): "+message +":"+ url);
    }

    private static void handleNoInternet() {
        isNoInternetHandled = true;
        //showDialog();
        Toast.makeText(mCtx, "No Internet", Toast.LENGTH_SHORT).show();
        Navigation.findNavController(mCtx.findViewById(R.id.nav_host_fragment)).navigate(
                Uri.parse("employeebuddy://nointernet")
        );
    }
/*
    private static void showDialog() {
        Dialog dialog = new Dialog(mCtx, R.style.AppTheme);
        dialog.setContentView(R.layout.fragment_no_internet);
        dialog.setCancelable(false);
        CardView close = dialog.findViewById(R.id.close);
        MaterialButton try_again = dialog.findViewById(R.id.try_again);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UniversalUtility.isNoInternetHandled = false;
               mCtx.finish();
            }
        });
        try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("clicked", "onClick: ");
                if (UniversalUtility.isOnline(mCtx)) {
                   dialog.dismiss();
                     Navigation.findNavController(mCtx.findViewById(R.id.nav_host_fragment)).navigateUp();

                } else
                    Toast.makeText(mCtx, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                UniversalUtility.isNoInternetHandled = false;

            }
        });
        dialog.show();
    }
*/

    public static boolean isOnline(Context context) {
        if (context!= null) {
            try {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                //should check null because in airplane mode it will be null
                return (netInfo != null && netInfo.isConnected());
            } catch (NullPointerException e) {
                e.printStackTrace();
                return false;
            }
        }else return false;
    }

    public static void saveFCMToken(String token) {
    }

    public static String formatDate(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }

    public static long getD0uration(Long startTime, Long endTime) {
        return  startTime - endTime;

    }

    public static class Paging{
        public static Executor getExecutor() {
            return Executors.newFixedThreadPool(5);

        }
        public static PagedList.Config getPagedListConfig(){

            return (new PagedList.Config.Builder())
                    .setEnablePlaceholders(true)
                    .setInitialLoadSizeHint(10)
                    .setPageSize(10)
                    .setPrefetchDistance(2)
                    .build();
        }
    }


    public static void showError(String string) {

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(string);
            JSONArray array = jsonObject.getJSONArray("messages");
            String message = array.getString(0);
            Toast.makeText(mCtx, message, Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static int calculateNumberOfDaysBetween(String start, String end) {
        Date startDate = yyyy_mm_ddToDate(start);
        Date endDate = yyyy_mm_ddToDate(end);
        if (startDate.after(endDate)) {
            Date temp = startDate;
            startDate = endDate;
            endDate = temp;
        }
        long startDateTime = startDate.getTime();
        long endDateTime = endDate.getTime();
        long milPerDay = 1000 * 60 * 60 * 24;

        int numOfDays = (int) ((endDateTime - startDateTime) / milPerDay); // calculate vacation duration in days

        return (numOfDays + 1); // add one day to include start date in interval
    }


    public static Date yyyy_mm_ddToDate(String datetxt) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(datetxt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String dateToyyyy_mm_dd(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static boolean ifLocationServiceEnabled() {
        int mode;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
// This is new method provided in API 28
            LocationManager lm = (LocationManager) mCtx.getSystemService(Context.LOCATION_SERVICE);
            return lm.isLocationEnabled();
        } else {
// This is Deprecated in API 28
            mode = Settings.Secure.getInt(mCtx.getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);

        }
        boolean b = (mode != Settings.Secure.LOCATION_MODE_OFF);

        if (!b) {
            // notify user
            new AlertDialog.Builder(mCtx)
                    .setMessage("Enable location service with high accuracy.")
                    .setPositiveButton("OK", (paramDialogInterface, paramInt) ->
                            mCtx.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    ).show();
        }
        return b;
    }


    public static void launchPlayStoreApp(Context context) {

        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }



    public static class Validator {
        public static boolean emailValidate(String s) {
            String expression = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            CharSequence inputStr = s;
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            return matcher.matches();
        }

        public static boolean phoneValidation(String s) {
            if (s.length() < 11 || s.length() > 15) {
                return false;
            }
            if (s.length() <= 14 && s.length() > 11) {
                s = s.substring(3, s.length() - 1);
            }
            if (s.length() == 15) {
                s = s.substring(4, s.length() - 1);
            }
            if (!s.startsWith("01"))
                return false;
            if (!TextUtils.isDigitsOnly(s))
                return false;
            Log.d("phone number", "phoneValidation: " + s);
            return s.length() == 11;
        }
    }

    public static int getVersionCode() {
        int targetSdkVersion = 0;
        int versionCode = 0;
        String versionName = "";
        try {
            PackageInfo packageInfo = mCtx.getPackageManager().getPackageInfo(mCtx.getPackageName(), 0);
            targetSdkVersion = packageInfo.applicationInfo.targetSdkVersion;
            versionCode = packageInfo.versionCode;
            versionName = packageInfo.versionName;
            Log.e("sdk_version code",versionCode+"");
            Log.e("sdk_version target ", targetSdkVersion+"");


        } catch (PackageManager.NameNotFoundException e) {
            Log.e("sdk_version", e.getMessage());
        }

        return versionCode;
    }
    public static void showAlert(String text) {
        String title = "Alert!!!";
        new AlertDialog.Builder(mCtx)
                .setTitle(title)
                .setMessage(text)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public static class ApiHelper{
        public static String getLocationApi()  {
            try {
                ApplicationInfo app = mCtx.getPackageManager().getApplicationInfo(mCtx.getPackageName(),PackageManager.GET_META_DATA);
                Bundle bundle = app.metaData;
                return bundle.getString("com.google.android.geo.API_KEY");
            }catch (Exception e) {
                return e.getMessage();
            }

        }

        public static String getPlacesApi() {
            try {
                ApplicationInfo app = mCtx.getPackageManager().getApplicationInfo(mCtx.getPackageName(),PackageManager.GET_META_DATA);
                Bundle bundle = app.metaData;
                return bundle.getString("com.google.android.places.API_KEY");
            }catch (Exception e) {
                return e.getMessage();
            }

        }
    }


    public static class Location{
        @RequiresApi(api = Build.VERSION_CODES.M)
        public static android.location.Location getLastKnownLocation() {
            android.location.Location l = null;
            LocationManager mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            List<String> providers = mLocationManager.getProviders(true);
            android.location.Location bestLocation = null;
            for (String provider : providers) {
                if (ActivityCompat.checkSelfPermission(mCtx,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(mCtx,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    l = mLocationManager.getLastKnownLocation(provider);

                }
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                }
            }
            if (bestLocation!= null)
                return bestLocation;
            else {
                return null;
            }
        }
    }
    public static boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
