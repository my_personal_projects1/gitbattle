package com.gsrajeni.githubbattle.core.retrofit;

import com.gsrajeni.githubbattle.model.developers_model.DeveloperModel;
import com.gsrajeni.githubbattle.model.repository_model.RepositoryModel;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;
import com.gsrajeni.githubbattle.model.users_repo_model.UsersRepoModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("https://github-trending-api.now.sh/repositories")
    Call<List<RepositoryModel>> getRepository(@QueryMap Map<String,String> map);

    @GET("search/users?q=repos:>42+followers:>1000")
    Call<DeveloperModel> getDevelopers(@QueryMap Map<String,String> map);

    @GET("/user")
    Call<UserDataModel> tryLogin(@HeaderMap Map<String, String> map);

    @GET("/users/{username}")
    Call<UserDataModel> getUserProfile(@Path("username") String username);

    @GET("https://api.github.com/users/{username}/repos")
    Call<List<UsersRepoModel>> getUsersRepo(@Path("username") String path);
}
