package com.gsrajeni.githubbattle.core;

import android.view.View;

public interface BaseInterface {
    void setupUI(View v);
    void init();
    void listeners();
    void observers();
}
