package com.gsrajeni.githubbattle.core.session;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.gsrajeni.githubbattle.core.util.UniversalUtility;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;

import java.util.HashMap;
import java.util.Map;
import static android.content.Context.MODE_PRIVATE;
import static android.content.Intent.makeRestartActivityTask;
import static com.gsrajeni.githubbattle.core.util.UniversalUtility.*;
public class SharedData {
    public static final String USER_DATA = "user_data";
    public static final String USER_TOKEN = "user_token";

    public static void setData(String key, String data){
        SharedPreferences.Editor editor = getActivity().getSharedPreferences("asdf", MODE_PRIVATE).edit();
        editor.putString(key, data);
        editor.apply();
    }

    public static Object getData(String key, Class<?> tClass){
        SharedPreferences prefs = mCtx.getSharedPreferences("asdf", MODE_PRIVATE);
        return new Gson().fromJson(prefs.getString(key, null), tClass);
    }

    public static void logoutUser(Activity context){
        SharedPreferences.Editor editor = context.getSharedPreferences("asdf", MODE_PRIVATE).edit();
        editor.putString(USER_DATA, null);
        editor.putString(USER_TOKEN, null);
        editor.apply();
        getActivity().startActivity(makeRestartActivityTask(getActivity().getIntent().getComponent()));
    }

    public static Map<String, String> getHeaders(Context mCtx){
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization",getData(USER_TOKEN, UserDataModel.class) == null ? null : "BASIC "+ getData(USER_TOKEN, String.class));
        return headers;
    }

}
