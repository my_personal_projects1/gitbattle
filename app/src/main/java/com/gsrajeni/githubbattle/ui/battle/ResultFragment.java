package com.gsrajeni.githubbattle.ui.battle;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.core.BaseInterface;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;

import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResultFragment extends Fragment implements BaseInterface {
    
    Button back;
    TextView winner_name,winner_username,winner_followers, winner_following,winner_public_repo, winner_total_points;
    TextView looser_name,looser_username,looser_followers, looser_following,looser_public_repo, looser_total_points;
    BattleViewModel viewModel;
    ImageView winner_image, looser_image;
    public ResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_result, container, false);
        setupUI(v);
        init();
        listeners();
        observers();
        return v;
    }

    @Override
    public void setupUI(View v) {
        winner_name = v.findViewById(R.id.winner_name);
        winner_username = v.findViewById(R.id.winner_username);
        winner_followers = v.findViewById(R.id.winner_followers);
        winner_following = v.findViewById(R.id.winner_following);
        winner_public_repo = v.findViewById(R.id.winner_public_repo);
        winner_total_points = v.findViewById(R.id.winner_total_points);

        looser_name = v.findViewById(R.id.looser_name);
        looser_username = v.findViewById(R.id.looser_username);
        looser_followers = v.findViewById(R.id.looser_followers);
        looser_following = v.findViewById(R.id.looser_following);
        looser_public_repo = v.findViewById(R.id.looser_public_repo);
        looser_total_points = v.findViewById(R.id.looser_total_points);

        winner_image = v.findViewById(R.id.winner_image);
        looser_image = v.findViewById(R.id.looser_image);

        back = v.findViewById(R.id.back);
    }

    @Override
    public void init() {
        viewModel = new ViewModelProvider(getActivity()).get(BattleViewModel.class);
    }

    @Override
    public void listeners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(getActivity().findViewById(R.id.nav_host_fragment))
                        .navigateUp();
            }
        });
    }

    @Override
    public void observers() {
        Map<String, UserDataModel> map = viewModel.compareResults();
        winner_name.setText("Name: "+(map.get("winner").getName()==null ? "Not found" : map.get("winner").getName()));
        winner_username.setText("Username: "+map.get("winner").getLogin());
        winner_followers.setText("Followers: "+ map.get("winner").getFollowers());
        winner_following.setText("Following: "+ map.get("winner").getFollowing());
        winner_public_repo.setText("Public Repos: " + map.get("winner").getPublicRepos());
        winner_total_points.setText("Total Points: "+ viewModel.calculatePoints(map.get("winner")));
        Glide.with(getActivity()).load(map.get("winner").getAvatarUrl()).placeholder(R.drawable.noresult).into(winner_image);

        looser_name.setText("Name: "+(map.get("looser").getName()==null ? "Not found" : map.get("looser").getName()));
        looser_username.setText("Username: " +map.get("looser").getLogin());
        looser_followers.setText("Followers: " + map.get("looser").getFollowers());
        looser_following.setText("Following: " + map.get("looser").getFollowing());
        looser_public_repo.setText("Public Repos: " + map.get("looser").getPublicRepos());
        looser_total_points.setText("Total Points: "+ viewModel.calculatePoints(map.get("looser")));
        Glide.with(getActivity()).load(map.get("looser").getAvatarUrl()).placeholder(R.drawable.noresult).into(looser_image);
    }
}