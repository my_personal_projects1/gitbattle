package com.gsrajeni.githubbattle.ui.profile;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gsrajeni.githubbattle.core.retrofit.ApiClient;
import com.gsrajeni.githubbattle.core.retrofit.ApiInterface;
import com.gsrajeni.githubbattle.core.util.LoadingDialog;
import com.gsrajeni.githubbattle.core.util.UniversalUtility;
import com.gsrajeni.githubbattle.model.users_repo_model.UsersRepoModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileRepository {
    Application application;
    ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
    public ProfileRepository(Application application) {
        this.application = application;
    }

    MutableLiveData<List<UsersRepoModel>> usersRepoList = new MutableLiveData<>();
    public void setUsersRepoList(String username){
        LoadingDialog dialog = new LoadingDialog(UniversalUtility.mCtx);
        dialog.startLoading();
        apiInterface.getUsersRepo(username).enqueue(new Callback<List<UsersRepoModel>>() {
            @Override
            public void onResponse(Call<List<UsersRepoModel>> call, Response<List<UsersRepoModel>> response) {
                dialog.endLoading();
                if (response.isSuccessful()){
                    usersRepoList.setValue(response.body());
                }else UniversalUtility.handleUnsuccessfulResponse(response);
            }

            @Override
            public void onFailure(Call<List<UsersRepoModel>> call, Throwable t) {
                dialog.endLoading();
                UniversalUtility.handleFailureResponse(t.getMessage(), call.request().url().toString());
            }
        });
    }
    public LiveData<List<UsersRepoModel>> getUsersRepo() {
        return usersRepoList;
    }
}
