package com.gsrajeni.githubbattle.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.model.repository_model.RepositoryModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RepositoryRecyclerAdapter extends RecyclerView.Adapter<RepositoryHolder> {

    private List<RepositoryModel> mDataset;
    private  Context mCtx;
    public RepositoryRecyclerAdapter(List<RepositoryModel> myDataset, Context mCtx) {
        this.mDataset = myDataset;
        this.mCtx = mCtx;
    }
    @NotNull
    @Override
    public RepositoryHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repository_recycler, parent, false);
        return new RepositoryHolder(view);
    }

    @Override
    public void onBindViewHolder(final RepositoryHolder holder, final int position) {
        holder.name.setText(mDataset.get(position).getAuthor()+"/"+mDataset.get(position).getName());
        holder.details.setText(mDataset.get(position).getDescription());
        holder.language.setText(mDataset.get(position).getLanguage());
        holder.total_stars.setText(mDataset.get(position).getStars().toString());
        holder.branches.setText(mDataset.get(position).getForks().toString());
        holder.total_stars_today.setText(mDataset.get(position).getCurrentPeriodStars().toString()+" stars today");
        Glide.with(mCtx).load(mDataset.get(position).getAvatar()).into(holder.built_by);
        holder.star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mCtx, "Feature is not implemented!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
class RepositoryHolder extends RecyclerView.ViewHolder {
    public TextView name, star, details, language, total_stars, branches, total_stars_today;
    public ImageView built_by;

    public RepositoryHolder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.name);
        star = itemView.findViewById(R.id.star);
        details = itemView.findViewById(R.id.details);
        language = itemView.findViewById(R.id.language);
        total_stars = itemView.findViewById(R.id.total_star);
        branches = itemView.findViewById(R.id.branches);
        total_stars_today = itemView.findViewById(R.id.total_stars_today);
        built_by = itemView.findViewById(R.id.built_by);
    }
}
