package com.gsrajeni.githubbattle.ui.login;

import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.core.BaseInterface;

public class LoginFragment extends Fragment implements BaseInterface {

    private LoginViewModel mViewModel;
    EditText userId, password;
    CardView signin;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment, container, false);
        setupUI(v);
        init();
        listeners();
        observers();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void setupUI(View v) {
        userId = v.findViewById(R.id.userid);
        password = v.findViewById(R.id.password);
        signin = v.findViewById(R.id.signin);
    }

    @Override
    public void init() {
        mViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

    }

    @Override
    public void listeners() {
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.tryLogin(userId.getText().toString(), password.getText().toString());
            }
        });
    }

    @Override
    public void observers() {
        mViewModel.isLoggedIn().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){
                    Navigation.findNavController(getActivity().findViewById(R.id.nav_host_fragment))
                            .navigate(R.id.action_loginFragment_to_profileFragment);

                }
            }
        });
    }
}