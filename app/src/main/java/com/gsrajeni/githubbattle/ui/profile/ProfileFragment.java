package com.gsrajeni.githubbattle.ui.profile;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.core.BaseInterface;
import com.gsrajeni.githubbattle.core.session.SharedData;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;
import com.gsrajeni.githubbattle.model.users_repo_model.UsersRepoModel;
import com.gsrajeni.githubbattle.ui.adapters.UsersRepoRecyclerAdapter;

import java.util.List;

public class ProfileFragment extends Fragment implements BaseInterface {

    private ProfileViewModel viewModel;
    Button sign_out, battle;
    RecyclerView users_repo_recycler;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_fragment, container, false);
        setupUI(v);
        init();
        listeners();
        observers();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void setupUI(View v) {
        sign_out = v.findViewById(R.id.log_out);
        battle = v.findViewById(R.id.battle);
        users_repo_recycler= v.findViewById(R.id.users_repo_recycler);
        users_repo_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void init() {
        viewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        UserDataModel model = (UserDataModel) SharedData.getData(SharedData.USER_DATA, UserDataModel.class);
        viewModel.setUsersRepo(model.getLogin());
    }

    @Override
    public void listeners() {
        sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedData.logoutUser(getActivity());
            }
        });
        battle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(getActivity().findViewById(R.id.nav_host_fragment))
                        .navigate(R.id.action_profileFragment_to_battleFragment);
            }
        });
    }

    @Override
    public void observers() {
        viewModel.getUsersRepo().observe(getViewLifecycleOwner(), new Observer<List<UsersRepoModel>>() {
            @Override
            public void onChanged(List<UsersRepoModel> usersRepoModels) {
                UsersRepoRecyclerAdapter adapter = new UsersRepoRecyclerAdapter(usersRepoModels, getActivity());
                users_repo_recycler.setAdapter(adapter);
            }
        });
    }
}