package com.gsrajeni.githubbattle.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.core.retrofit.ApiClient;
import com.gsrajeni.githubbattle.core.retrofit.ApiInterface;
import com.gsrajeni.githubbattle.model.developers_model.DeveloperModel;
import com.gsrajeni.githubbattle.model.developers_model.Item;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeveloperRecyclerAdapter extends RecyclerView.Adapter<DeveloperHolder> {

    private DeveloperModel mDataset;
    private Context mCtx;

    public DeveloperRecyclerAdapter(DeveloperModel myDataset, Context mCtx) {
        this.mDataset = myDataset;
        this.mCtx = mCtx;
    }

    @NotNull
    @Override
    public DeveloperHolder onCreateViewHolder(ViewGroup parent,
                                              int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.developer_recycler, parent, false);
        return new DeveloperHolder(view);
    }

    @Override
    public void onBindViewHolder(final DeveloperHolder holder, final int position) {
        List<Item> items = mDataset.getItems();
        ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        apiInterface.getUserProfile(items.get(position).getLogin()).enqueue(new Callback<UserDataModel>() {
            @Override
            public void onResponse(Call<UserDataModel> call, Response<UserDataModel> response) {
                if (response.isSuccessful()){
                    if (response.body().getName() == null)
                        holder.name.setText(items.get(position).getLogin());
                    else holder.name.setText(response.body().getName());
                }
            }

            @Override
            public void onFailure(Call<UserDataModel> call, Throwable t) {
                holder.name.setText("No name found");
            }
        });

        holder.login.setText(items.get(position).getLogin());
        holder.repository.setText(items.get(position).getReposUrl());
        Glide.with(mCtx).load(items.get(position).getAvatarUrl()).placeholder(R.drawable.noresult).into(holder.avatar);
        holder.follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mCtx, "Feature is not implemented!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDataset.getItems().size();
    }
}

class DeveloperHolder extends RecyclerView.ViewHolder {
    public TextView name, login, repository;
    Button follow;
    public ImageView avatar;

    public DeveloperHolder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.name);
        login = itemView.findViewById(R.id.login);
        repository = itemView.findViewById(R.id.repo_name);
        follow = itemView.findViewById(R.id.follow);
        avatar = itemView.findViewById(R.id.avatar);
    }
}
