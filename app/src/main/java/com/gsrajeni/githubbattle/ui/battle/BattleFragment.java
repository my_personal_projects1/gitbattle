package com.gsrajeni.githubbattle.ui.battle;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.core.BaseInterface;
import com.gsrajeni.githubbattle.core.session.SharedData;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;

public class BattleFragment extends Fragment implements BaseInterface {

    private BattleViewModel mViewModel;

    Button submit, choose_me, checkuser1, checkuser2;
    EditText user_1_username, user_2_username;
    TextView user_1_name, user_2_name;
    ImageView user_1_image,user_2_image;

    public static BattleFragment newInstance() {
        return new BattleFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.battle_fragment, container, false);
        setupUI(v);
        init();
        listeners();
        observers();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void setupUI(View v) {
        submit = v.findViewById(R.id.submit);
        user_1_name = v.findViewById(R.id.user_1_name);
        user_2_name = v.findViewById(R.id.user_2_name);
        user_1_image = v.findViewById(R.id.user_1_image);
        user_2_image = v.findViewById(R.id.user_2_image);
        user_1_username = v.findViewById(R.id.user_1_username);
        user_2_username = v.findViewById(R.id.user_2_username);
        choose_me = v.findViewById(R.id.choose_me);
        checkuser1 = v.findViewById(R.id.checkuser1);
        checkuser2 = v.findViewById(R.id.checkuser2);

    }

    @Override
    public void init() {
        mViewModel = new ViewModelProvider(getActivity()).get(BattleViewModel.class);
    }

    @Override
    public void listeners() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mViewModel.isUsersReady())
                Navigation.findNavController(getActivity().findViewById(R.id.nav_host_fragment))
                        .navigate(R.id.resultFragment);
                else Toast.makeText(getActivity(), "Please fill two users first!", Toast.LENGTH_SHORT).show();
            }
        });
        choose_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserDataModel model = (UserDataModel) SharedData.getData(SharedData.USER_DATA, UserDataModel.class);
                user_1_username.setText(model.getLogin());
                mViewModel.setUser1Profile(user_1_username.getText().toString());
            }
        });
        checkuser1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.setUser1Profile(user_1_username.getText().toString());
            }
        });
        checkuser2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewModel.setUser2Profile(user_2_username.getText().toString());
            }
        });
    }

    @Override
    public void observers() {
        mViewModel.getUser1Profile().observe(getViewLifecycleOwner(), new Observer<UserDataModel>() {
            @Override
            public void onChanged(UserDataModel userDataModel) {
                if(userDataModel!= null) {
                    user_1_name.setText(userDataModel.getName()==null ? userDataModel.getLogin() : userDataModel.getName());
                    Glide.with(getActivity()).load(userDataModel.getAvatarUrl()).into(user_1_image);
                }
                else user_1_name.setText("User not found");
            }
        });
        mViewModel.getUser2Profile().observe(getViewLifecycleOwner(), new Observer<UserDataModel>() {
            @Override
            public void onChanged(UserDataModel userDataModel) {
                if(userDataModel != null) {
                    user_2_name.setText(userDataModel.getName()==null ? userDataModel.getLogin() : userDataModel.getName());
                    Glide.with(getActivity()).load(userDataModel.getAvatarUrl())
                            .placeholder(R.drawable.noresult)
                            .into(user_2_image);
                }
                else {
                    user_2_name.setText("User not found!");
                }

            }
        });
    }
}