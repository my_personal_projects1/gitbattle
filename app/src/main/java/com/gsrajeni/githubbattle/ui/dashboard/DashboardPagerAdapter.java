package com.gsrajeni.githubbattle.ui.dashboard;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.gsrajeni.githubbattle.ui.dashboard.developers.DevelopersFragment;
import com.gsrajeni.githubbattle.ui.dashboard.repositories.RepositoriesFragment;

import java.util.ArrayList;
import java.util.List;

public class DashboardPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    private final List<String> tabTitles = new ArrayList<String>() {{
        add("Repositories");
        add("Developers");
    }};

    private List<Fragment> tabs = new ArrayList<>();


    public DashboardPagerAdapter(FragmentManager fm) {
        super(fm);
        initializeTabs();

    }
    private void initializeTabs() {
        tabs.add(new RepositoriesFragment());
        tabs.add(new DevelopersFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return tabs.get(position);
    }

    @Override
    public int getCount() {

        return tabs.size();

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }
}