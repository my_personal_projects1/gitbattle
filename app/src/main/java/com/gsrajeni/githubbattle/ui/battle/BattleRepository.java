package com.gsrajeni.githubbattle.ui.battle;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gsrajeni.githubbattle.core.retrofit.ApiClient;
import com.gsrajeni.githubbattle.core.retrofit.ApiInterface;
import com.gsrajeni.githubbattle.core.util.LoadingDialog;
import com.gsrajeni.githubbattle.core.util.UniversalUtility;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BattleRepository {
    Application application;
    MutableLiveData<Boolean> is_created = new MutableLiveData<>();
    ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
    MutableLiveData<UserDataModel> user1profile = new MutableLiveData<>();
    MutableLiveData<UserDataModel> user2profile = new MutableLiveData<>();
    public BattleRepository(Application application) {
        this.application = application;
    }

    public LiveData<UserDataModel> getUser1Profile(){
        return user1profile;
    }
    public void setUser1Profile(String username){
        LoadingDialog dialog = new LoadingDialog(UniversalUtility.mCtx);
        dialog.startLoading();
        apiInterface.getUserProfile(username).enqueue(new Callback<UserDataModel>() {
            @Override
            public void onResponse(Call<UserDataModel> call, Response<UserDataModel> response) {
                dialog.endLoading();
                if(response.isSuccessful())
                    user1profile.setValue(response.body());
                else user1profile.setValue(null);

            }

            @Override
            public void onFailure(Call<UserDataModel> call, Throwable t) {
                dialog.endLoading();
                UniversalUtility.handleFailureResponse(call.request().url().toString(), t.getMessage());
            }
        });
    }

    public LiveData<UserDataModel> getUser2Profile(){
        return user2profile;
    }
    public void setUser2Profile(String username){
        LoadingDialog dialog = new LoadingDialog(UniversalUtility.mCtx);
        dialog.startLoading();
        apiInterface.getUserProfile(username).enqueue(new Callback<UserDataModel>() {
            @Override
            public void onResponse(Call<UserDataModel> call, Response<UserDataModel> response) {
                dialog.endLoading();
                if(response.isSuccessful())
                    user2profile.setValue(response.body());
                else user2profile.setValue(null);

            }

            @Override
            public void onFailure(Call<UserDataModel> call, Throwable t) {
                dialog.endLoading();
                UniversalUtility.handleFailureResponse(call.request().url().toString(), t.getMessage());
            }
        });
    }
}
