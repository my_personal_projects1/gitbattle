package com.gsrajeni.githubbattle.ui.dashboard;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.gsrajeni.githubbattle.model.developers_model.DeveloperModel;
import com.gsrajeni.githubbattle.model.users_repo_model.UsersRepoModel;
import com.gsrajeni.githubbattle.ui.dashboard.DashboardRepository;
import com.gsrajeni.githubbattle.model.repository_model.RepositoryModel;

import java.util.List;
import java.util.Map;

public class DashboardViewModel extends AndroidViewModel {
    DashboardRepository repository;
    public DashboardViewModel(@NonNull Application application) {
        super(application);
        repository = new DashboardRepository(application);
    }
    public void setRepository(Map<String,String> map){
        repository.setRepositoryList(map);
    }
    public LiveData<List<RepositoryModel>> getRepository() {
        return repository.getRepositoryList();
    }

    public void setDevelopers(Map<String,String> map){
        repository.setDeveloperList(map);
    }
    public LiveData<DeveloperModel> getDevelopers(){
        return repository.getDeveloperList();
    }
}