package com.gsrajeni.githubbattle.ui.battle;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;

import java.util.HashMap;
import java.util.Map;

public class BattleViewModel extends AndroidViewModel {
     BattleRepository repository;
    public BattleViewModel(@NonNull Application application) {
        super(application);
        repository = new BattleRepository(application);
    }

    public void setUser1Profile(String username) {
        repository.setUser1Profile(username);
    }
    public LiveData<UserDataModel> getUser1Profile(){
        return repository.getUser1Profile();
    }


    public void setUser2Profile(String username) {
        repository.setUser2Profile(username);
    }
    public LiveData<UserDataModel> getUser2Profile(){
        return repository.getUser2Profile();
    }

    public Map<String, UserDataModel> compareResults() {
        int user1Point = calculatePoints(repository.getUser1Profile().getValue());
        int user2Point = calculatePoints(repository.getUser2Profile().getValue());
        Map<String, UserDataModel> map = new HashMap<>();
        if(user1Point <= user2Point) {
            map.put("winner", repository.getUser2Profile().getValue());
            map.put("looser", repository.getUser1Profile().getValue());
        }else{
            map.put("winner", repository.getUser1Profile().getValue());
            map.put("looser", repository.getUser2Profile().getValue());
        }
        return map;
    }

    public int calculatePoints(UserDataModel value) {
        int points = value.getFollowers()+value.getFollowing()+value.getPublicRepos();
        return (int)points/2;
    }

    public Boolean isUsersReady() {
        if(getUser1Profile().getValue()!= null && getUser2Profile().getValue()!= null){
            return true;
        }else return  false;
    }


    // TODO: Implement the ViewModel
}