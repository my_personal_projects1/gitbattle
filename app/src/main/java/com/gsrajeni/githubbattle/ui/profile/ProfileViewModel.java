package com.gsrajeni.githubbattle.ui.profile;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.gsrajeni.githubbattle.model.users_repo_model.UsersRepoModel;

import java.util.List;

public class ProfileViewModel extends AndroidViewModel {
    ProfileRepository repository;
    public ProfileViewModel(@NonNull Application application) {
        super(application);
        repository = new ProfileRepository(application);
    }
    public void setUsersRepo(String username){
        repository.setUsersRepoList(username);
    }
    public LiveData<List<UsersRepoModel>> getUsersRepo(){
        return repository.getUsersRepo();
    }

}