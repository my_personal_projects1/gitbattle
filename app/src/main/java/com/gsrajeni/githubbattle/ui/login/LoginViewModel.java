package com.gsrajeni.githubbattle.ui.login;

import android.app.Application;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class LoginViewModel extends AndroidViewModel {
    LoginRepository repository;
    public LoginViewModel(@NonNull Application application) {
        super(application);
        repository = new LoginRepository(application);
    }

    public void tryLogin(String userId, String password) {
        if(validate(userId, password)){
            repository.tryLogin(userId, password);
        }
    }
    public LiveData<Boolean> isLoggedIn(){
        return repository.getIsLoggedIn();
    }

    private boolean validate(String userId, String password) {
        if(TextUtils.isEmpty(userId))
            return false;
        else return !TextUtils.isEmpty(password);
    }
    // TODO: Implement the ViewModel
}