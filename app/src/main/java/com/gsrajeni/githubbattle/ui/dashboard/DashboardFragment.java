package com.gsrajeni.githubbattle.ui.dashboard;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.core.BaseInterface;
import com.gsrajeni.githubbattle.core.session.SharedData;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;
import com.gsrajeni.githubbattle.model.users_repo_model.UsersRepoModel;
import com.gsrajeni.githubbattle.ui.adapters.UsersRepoRecyclerAdapter;

import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements BaseInterface {
    TabLayout tabLayout;
    ViewPager viewPager;
    DashboardViewModel viewModel;
    Button login;
    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.dashboard_fragment, container, false);
        setupUI(v);
        init();
        listeners();
        observers();
        return v;
    }

    @Override
    public void init() {
        viewModel = new ViewModelProvider(this).get(DashboardViewModel.class);
        viewModel.setRepository(new HashMap<>());
        viewModel.setDevelopers(new HashMap<>());
        DashboardPagerAdapter pagerAdapter = new DashboardPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        if(SharedData.getData(SharedData.USER_TOKEN, String.class)!= null){
            Navigation.findNavController(getActivity().findViewById(R.id.nav_host_fragment))
                    .navigate(R.id.action_dashboardFragment_to_profileFragment);
        }
    }

    @Override
    public void setupUI(View v) {
        viewPager = v.findViewById(R.id.pager);
        tabLayout = v.findViewById(R.id.tab_layout);
        login = v.findViewById(R.id.login);
    }

    @Override
    public void listeners() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(getActivity().findViewById(R.id.nav_host_fragment))
                        .navigate(R.id.action_dashboardFragment_to_loginFragment);
            }
        });
    }

    @Override
    public void observers() {

    }
}
