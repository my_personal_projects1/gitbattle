package com.gsrajeni.githubbattle.ui.dashboard;


import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gsrajeni.githubbattle.core.retrofit.ApiClient;
import com.gsrajeni.githubbattle.core.retrofit.ApiInterface;
import com.gsrajeni.githubbattle.core.util.LoadingDialog;
import com.gsrajeni.githubbattle.core.util.UniversalUtility;
import com.gsrajeni.githubbattle.model.developers_model.DeveloperModel;
import com.gsrajeni.githubbattle.model.repository_model.RepositoryModel;
import com.gsrajeni.githubbattle.model.users_repo_model.UsersRepoModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.PUT;

public class DashboardRepository {
    Application application;
    MutableLiveData<Boolean> is_created = new MutableLiveData<>();
    ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
    public DashboardRepository(Application application) {
        this.application = application;
    }
    MutableLiveData<List<RepositoryModel>> repositoryList = new MutableLiveData<>();
    MutableLiveData<DeveloperModel> developerList = new MutableLiveData<>();
    public void setRepositoryList(Map <String, String> map){
        LoadingDialog dialog = new LoadingDialog(UniversalUtility.mCtx);
        dialog.startLoading();
        apiInterface.getRepository(map).enqueue(new Callback<List<RepositoryModel>>() {
            @Override
            public void onResponse(Call<List<RepositoryModel>> call, Response<List<RepositoryModel>> response) {
                dialog.endLoading();
                if(response.isSuccessful())
                    repositoryList.setValue(response.body());
                else UniversalUtility.handleUnsuccessfulResponse(response);
            }

            @Override
            public void onFailure(Call<List<RepositoryModel>> call, Throwable t) {
                dialog.endLoading();
                UniversalUtility.handleFailureResponse(t.getMessage(), call.request().url().toString());
            }
        });
    }

    public void setDeveloperList(Map <String, String> map){
        LoadingDialog dialog = new LoadingDialog(UniversalUtility.mCtx);
        dialog.startLoading();
        apiInterface.getDevelopers(map).enqueue(new Callback<DeveloperModel>() {
            @Override
            public void onResponse(Call<DeveloperModel> call, Response<DeveloperModel> response) {
                dialog.endLoading();
                if(response.isSuccessful())
                    developerList.setValue(response.body());
                else UniversalUtility.handleUnsuccessfulResponse(response);

            }

            @Override
            public void onFailure(Call<DeveloperModel> call, Throwable t) {
                dialog.endLoading();
                UniversalUtility.handleFailureResponse(t.getMessage(), call.request().url().toString());
            }
        });
    }
    
    public LiveData<List<RepositoryModel>> getRepositoryList(){
        return repositoryList;
    }

    public LiveData<DeveloperModel> getDeveloperList(){
        return developerList;
    }


}
