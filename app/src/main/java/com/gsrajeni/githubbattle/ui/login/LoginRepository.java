package com.gsrajeni.githubbattle.ui.login;

import android.app.Application;
import android.util.Base64;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.gsrajeni.githubbattle.core.retrofit.ApiClient;
import com.gsrajeni.githubbattle.core.retrofit.ApiInterface;
import com.gsrajeni.githubbattle.core.session.SharedData;
import com.gsrajeni.githubbattle.core.util.LoadingDialog;
import com.gsrajeni.githubbattle.core.util.UniversalUtility;
import com.gsrajeni.githubbattle.model.user_data_model.UserDataModel;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {
    Application application;
    MutableLiveData<Boolean> isLoggedIn = new MutableLiveData<>();
    ApiInterface apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
    public LoginRepository(Application application) {
        this.application = application;
    }
    public void tryLogin(String userid, String password){
        String encode = Base64.encodeToString((userid + ":" + password).getBytes(), Base64.DEFAULT).replace("\n", "");
        Map<String, String> map = new HashMap<>();
        map.put("Authorization", "Basic "+ encode);
        LoadingDialog dialog = new LoadingDialog(UniversalUtility.mCtx);
        dialog.startLoading();
        apiInterface.tryLogin(map).enqueue(new Callback<UserDataModel>() {
            @Override
            public void onResponse(Call<UserDataModel> call, Response<UserDataModel> response) {
                dialog.endLoading();
                if(response.isSuccessful()){
                    SharedData.setData(SharedData.USER_DATA,new Gson().toJson(response.body()));
                    SharedData.setData(SharedData.USER_TOKEN, new Gson().toJson(encode));
                    Toast.makeText(application, "User logged in successfully!", Toast.LENGTH_SHORT).show();
                    isLoggedIn.setValue(true);
                }else {
                    UniversalUtility.handleUnsuccessfulResponse(response);
                    isLoggedIn.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<UserDataModel> call, Throwable t) {
                dialog.endLoading();
                UniversalUtility.handleFailureResponse(t.getMessage(), call.request().url().toString());
                isLoggedIn.setValue(false);
            }
        });
    }

    public LiveData<Boolean> getIsLoggedIn() {
        return isLoggedIn;
    }
}
