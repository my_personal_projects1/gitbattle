package com.gsrajeni.githubbattle.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.model.repository_model.RepositoryModel;
import com.gsrajeni.githubbattle.model.users_repo_model.UsersRepoModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UsersRepoRecyclerAdapter extends RecyclerView.Adapter<UsersRepoHolder> {

    private List<UsersRepoModel> mDataset;
    private  Context mCtx;
    CardView repoCard;
    public UsersRepoRecyclerAdapter(List<UsersRepoModel> myDataset, Context mCtx) {
        this.mDataset = myDataset;
        this.mCtx = mCtx;
    }
    @NotNull
    @Override
    public UsersRepoHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.users_repo_recycler, parent, false);
        return new UsersRepoHolder(view);
    }

    @Override
    public void onBindViewHolder(final UsersRepoHolder holder, final int position) {
        holder.name.setText(mDataset.get(position).getFullName());
        holder.star.setText(mDataset.get(position).getStargazersCount().toString());
        holder.repoCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mCtx, "Feature is not implemented yet!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

class UsersRepoHolder extends RecyclerView.ViewHolder {
    public TextView name, star;
    CardView repoCard;

    public UsersRepoHolder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.name);
        star = itemView.findViewById(R.id.star);
        repoCard = itemView.findViewById(R.id.repo_card);
    }
}
