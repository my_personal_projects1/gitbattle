package com.gsrajeni.githubbattle.ui.dashboard.developers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gsrajeni.githubbattle.R;
import com.gsrajeni.githubbattle.core.BaseInterface;
import com.gsrajeni.githubbattle.model.developers_model.DeveloperModel;
import com.gsrajeni.githubbattle.model.repository_model.RepositoryModel;
import com.gsrajeni.githubbattle.ui.adapters.DeveloperRecyclerAdapter;
import com.gsrajeni.githubbattle.ui.adapters.RepositoryRecyclerAdapter;
import com.gsrajeni.githubbattle.ui.dashboard.DashboardViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DevelopersFragment extends Fragment implements BaseInterface {

    DashboardViewModel viewModel;
    RecyclerView repository_recycler;
    Spinner date_range;
    Map<String,String> map;
    public static DevelopersFragment newInstance() {
        return new DevelopersFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.repositories_fragment, container, false);
        setupUI(v);
        init();
        listeners();
        observers();
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void setupUI(View v) {
        repository_recycler = v.findViewById(R.id.repository_recycler);
        date_range = v.findViewById(R.id.date_range);
        repository_recycler.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void init() {
        viewModel = new ViewModelProvider(getParentFragment()).get(DashboardViewModel.class);
        map = new HashMap<>();
        map.put("q","sort=stars");
        map.put("order", "desc");
        List<String> filter = new ArrayList<>();
        filter.add("Daily");
        filter.add("Weekly");
        filter.add("Monthly");
        date_range.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, filter));
    }

    @Override
    public void listeners() {
        date_range.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    map.put("since", ((TextView) view).getText().toString().toLowerCase());
                    viewModel.setRepository(map);
                }catch (Exception e){}
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void observers() {
        viewModel.getDevelopers().observe(getViewLifecycleOwner(), new Observer<DeveloperModel>() {
            @Override
            public void onChanged(DeveloperModel models) {
                DeveloperRecyclerAdapter adapter =
                        new DeveloperRecyclerAdapter(models, getActivity());
                repository_recycler.setAdapter(adapter);
            }
        });
    }

}